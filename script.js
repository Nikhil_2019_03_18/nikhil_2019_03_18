var count = 0; //number of canvases

function init() {
	var select = document.getElementById("canvasNumber");
	select.innerHTML = "";  //reset to empty when button is clicked
	var defaultOpt = document.createElement("option");
	defaultOpt.value = "Canvas #";
   	defaultOpt.innerHTML = "Canvas #";
   	select.appendChild(defaultOpt);
}

function generateRandomCanvases() {
	count = getRandomNumber(2,4);
	var container = document.getElementById("container");
	container.innerHTML = ""; //reset to empty when button is clicked
	var select = document.getElementById("canvasNumber");
	select.innerHTML = "";  //reset to empty when button is clicked
	var defaultOpt = document.createElement("option");
	defaultOpt.value = "Canvas #";
   	defaultOpt.innerHTML = "Canvas #";
   	select.appendChild(defaultOpt);
	for(var i=0; i<count; i++) {
		var newCanvas = document.createElement("canvas");
		newCanvas.id = "canvas" + (i+1);
		newCanvas.width = 250;
		newCanvas.height = 250;
		container.appendChild(newCanvas);
		var fabricCanvas = new fabric.Canvas(newCanvas, {width: 250, height: 250});
		var opt = document.createElement("option");
		opt.value = i + 1;
   		opt.innerHTML = i + 1;
   		select.appendChild(opt);
	}
}

//get random number between fromValue and toValue
function getRandomNumber(fromValue, toValue) {
	var number;
	if(fromValue < 0 || toValue < 0) {
		return -1;
	}
	while(true){
		number = Math.floor(Math.random() * (toValue+1))
		if (number < fromValue) {
			continue;
		} else {
			break;
		}
	}
	return number;
}

function insert() {
	var selectValue = document.getElementById("canvasNumber").value;
	if(count === 0) {
		alert("Please generate canvas(es) before you use this option!");
	} else if(selectValue === "Canvas #") {
		alert("You need to select a Canvas number first!");
	} else {
		getExternalObjects().then(function(response){
			var random = getRandomNumber(2, response.length - 3); // these arguments will omit the first two and last two objects
			var selected = [0, 1, random, response.length-2, response.length-1]; //index of first two, last 2 and one object randomly selected
			var canvas = new fabric.Canvas("canvas"+selectValue);
			var url = "";
			for(let i=0;i<selected.length;i++) {
				if((response[selected[i]].id)%2 === 1) { //id is odd
					url = response[selected[i]].thumbnailUrl;
					fabric.Image.fromURL(url, function(oImg) {
					  oImg.scaleToWidth(40).set('top', i*50+22).set('left', 5);
					  canvas.add(oImg);
					});
				} else if((response[selected[i]].id)%2 === 0) { //id is even
					var text = new fabric.Text(response[selected[i]].title, { left: 5, top: i*50+22, fontSize: 12 });
					canvas.add(text);
				}

				if(response[selected[i]].albumId >= 100) { //albumId >= 100
					var text = new fabric.Text(response[selected[i]].url, { left: 10, top: i*50+10, fontSize: 12 });
					canvas.add(text);
				}
				
			}
			
		}, function(err){
			alert("There was some error while fetching the remote objects!")
		});
	}

}

function getExternalObjects() {
	return new Promise(function(resolve, reject) {
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if(this.readyState==4 && this.status==200) {
				resolve(JSON.parse(this.response));
			} else if(this.readyState==4) {
				reject("Some error occured!");
			}
		};
		xhttp.open("GET", "https://jsonplaceholder.typicode.com/photos", true);
		xhttp.send();
	});	
}

